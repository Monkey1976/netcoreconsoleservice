using System;
using System.Net;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using ConsoleService.Configuration;


namespace ConsoleService.Service
{
    public class ServiceStart
    {
        private readonly ILogger<ServiceStart> _logger;
        private readonly IMyConfig _config;

        public ServiceStart(IMyConfig configurationRoot, ILogger<ServiceStart> logger)
        {
            _logger = logger;
            _config = configurationRoot;
        }

        /// Code here
        public void Run()
        {
            // Run tasks
            
        }

      
    }
}