using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using ConsoleService.Service;
using ConsoleService.Configuration;
using System.Collections.Generic;

namespace ConsoleService
{
   public class Startup
{

    IConfigurationRoot Configuration { get; }

    public Startup()
    {
        var builder = new ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
          .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);
          

        Configuration = builder.Build();
    }


    public void ConfigureServices(IServiceCollection services)
    {
        services.AddSingleton<IConfigurationRoot>(Configuration);
        services.AddSingleton<IMyConfig, MyConfig>();
        services.AddSingleton(new LoggerFactory()
                .AddNLog());

        NLog.LogManager.LoadConfiguration("NLog.config");

        services.AddLogging();
        services.AddTransient<ServiceStart>();        
    }

}
}
