using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace ConsoleService.Configuration
{
    public class MyConfig : IMyConfig
    {
        IConfigurationRoot _configurationRoot;
        public MyConfig(IConfigurationRoot configurationRoot)
        {
            _configurationRoot = configurationRoot;
        }
        public string testParam => _configurationRoot["testParam"];
        
        
    }   
}